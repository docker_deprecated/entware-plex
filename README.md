# entware-plex

#### [entware-x64-plex](https://hub.docker.com/r/forumi0721entwarex64/entware-x64-plex/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721entwarex64/entware-x64-plex/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721entwarex64/entware-x64-plex/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721entwarex64/entware-x64-plex/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721entwarex64/entware-x64-plex)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721entwarex64/entware-x64-plex)
#### [entware-aarch64-plex](https://hub.docker.com/r/forumi0721entwareaarch64/entware-aarch64-plex/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721entwareaarch64/entware-aarch64-plex/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721entwareaarch64/entware-aarch64-plex/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721entwareaarch64/entware-aarch64-plex/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721entwareaarch64/entware-aarch64-plex)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721entwareaarch64/entware-aarch64-plex)
#### [entware-armhf-plex](https://hub.docker.com/r/forumi0721entwarearmhf/entware-armhf-plex/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721entwarearmhf/entware-armhf-plex/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721entwarearmhf/entware-armhf-plex/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721entwarearmhf/entware-armhf-plex/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721entwarearmhf/entware-armhf-plex)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721entwarearmhf/entware-armhf-plex)



----------------------------------------
#### Description

* Distribution : [Entware](https://github.com/Entware/Entware/)
* Architecture : x64,aarch64,armhf
* Appplication : [Plex Media Server](https://www.plex.tv/)
    - The Plex Media Server either running on Windows, macOS, Linux, FreeBSD or a NAS which organizes audio (music) and visual (photos and videos) content from personal media libraries and streams it to their player counterparts.



----------------------------------------
#### Run

```sh
docker run -d \
           --net=host \
		   -p 32400:32400/tcp \
		   -p 3005:3005/tcp \
		   -p 8324:8324/tcp \
		   -p 32469:32469/tcp \
		   -p 1900:1900/udp \
		   -p 32410:32410/udp \
		   -p 32412:32412/udp \
		   -p 32413:32413/udp \
		   -p 32414:32414/udp \
		   -v /comf.d:/conf.d \
		   -v /transcode:/transcode \
		   -v /data:/data \
		   forumi0721entware[ARCH]/entware-[ARCH]-plex:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:32400/](http://localhost:32400/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --net=host         | for Broadcast                                    |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 32400/tcp          | Access port                                      |
| 3005/tcp           | Plex port                                        |
| 8324/tcp           | Plex port                                        |
| 32469/tcp          | Plex port                                        |
| 1900/udp           | Plex port                                        |
| 32410/udp          | Plex port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |
| /data              | Media data                                       |
| /transcode         | Transcode data                                   |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

